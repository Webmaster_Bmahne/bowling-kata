/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import bowlingkata.Game;

import org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.is;
import org.hamcrest.MatcherAssert.*;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Before;
import org.junit.Ignore;

import org.junit.Test;

/**
 *
 * @author boris
 */
public class BowlingKataTest {

    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void StrikeTest() {

        assertThat(game.rolling(10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10), is(300));
    }

    @Test
   
    public void SpareTest() {
        assertThat(game.rolling(5,5, 5,5, 5,5, 5,5, 5,5, 5,5, 5,5, 5,5, 5,5, 5,5,5), is(150));
    }

    @Test
   
     public void ZeroGameTest() {
        assertThat(game.rolling(9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), is(90));
    }
 
   
  

}
